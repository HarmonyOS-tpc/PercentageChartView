/*
 * Copyright 2018 Rami Jemli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http:// www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ramijemli.percentagechartview.renderer;

import com.ramijemli.percentagechartview.IPercentageChartView;
import com.ramijemli.percentagechartview.callback.AdaptiveColorProvider;
import ohos.agp.components.AttrSet;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.LinearShader;
import ohos.agp.render.Paint;
import ohos.agp.render.RadialShader;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;

/**
 * Fill mode renderer
 */
public class FillModeRenderer extends BaseModeRenderer implements OffsetEnabledMode {
    /**
     * The constant M direction angle
     */
    private float mDirectionAngle;
    /**
     * The constant M bg sweep angle
     */
    private float mBgSweepAngle;
    /**
     * The constant M radius
     */
    private float mRadius;

    /**
     * Fill mode renderer
     *
     * @param view view
     */
    public FillModeRenderer(IPercentageChartView view) {
        super(view);
        setup();
    }

    /**
     * Fill mode renderer
     *
     * @param view  view
     * @param attrs attrs
     */
    public FillModeRenderer(IPercentageChartView view, AttrSet attrs) {
        super(view, attrs);
        setup();
    }

    /**
     * Setup
     */
    @Override
    void setup() {
        super.setup();
        this.mDirectionAngle = mStartAngle;
    }

    /**
     * Measure *
     *
     * @param width         width
     * @param height        height
     * @param paddingLeft   padding left
     * @param paddingTop    padding top
     * @param paddingRight  padding right
     * @param paddingBottom padding bottom
     */
    @Override
    public void measure(int width, int height, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        int centerX = width / 2;
        int centerY = height / 2;
        mRadius = (float) Math.min(width, height) / 2;

        mCircleBounds = new RectFloat(centerX - mRadius,
                centerY - mRadius,
                centerX + mRadius,
                centerY + mRadius);
        measureBackgroundBounds();
        updateDrawingAngles();
        setupGradientColors(mCircleBounds);
        updateText();
    }

    /**
     * Measure background bounds
     */
    private void measureBackgroundBounds() {
        mBackgroundBounds = new RectFloat(mCircleBounds.left + mBackgroundOffset,
                mCircleBounds.top + mBackgroundOffset,
                mCircleBounds.right - mBackgroundOffset,
                mCircleBounds.bottom - mBackgroundOffset);
    }

    /**
     * Draw *
     *
     * @param canvas canvas
     */
    @Override
    public void draw(Canvas canvas) {
        // BACKGROUND
        if (mDrawBackground) {
            canvas.drawArc(mBackgroundBounds, new Arc(mStartAngle, mBgSweepAngle, false), mBackgroundPaint);
        }

        // FOREGROUND
        canvas.drawArc(mCircleBounds, new Arc(mStartAngle, mSweepAngle, false), mProgressPaint);

        // TEXT
        drawText(canvas);
    }

    /**
     * Set adaptive color provider *
     *
     * @param adaptiveColorProvider adaptive color provider
     */
    @Override
    public void setAdaptiveColorProvider(AdaptiveColorProvider adaptiveColorProvider) {
        if (adaptiveColorProvider == null) {
            mProgressColorAnimator = mBackgroundColorAnimator = mTextColorAnimator = null;
            this.mAdaptiveColorProvider = null;
            mTextPaint.setColor(new Color(mTextColor));
            mBackgroundPaint.setColor(new Color(mBackgroundColor));
            mProgressPaint.setColor(new Color(mProgressColor));
            mView.postInvalidate();
            return;
        }

        this.mAdaptiveColorProvider = adaptiveColorProvider;

        setupColorAnimations();
        updateProvidedColors(mProgress);
        mView.postInvalidate();
    }

    /**
     * Setup gradient colors *
     *
     * @param bounds bounds
     */
    @Override
    void setupGradientColors(RectFloat bounds) {
        if (mGradientType == -1 || mGradientType == GRADIENT_SWEEP) {
            return;
        }

        switch (mGradientType) {
            default:
            case GRADIENT_LINEAR:
                Point[] points = new Point[]{new Point(bounds.getCenter().getPointX(), bounds.top), new Point(bounds.getCenter().getPointX(), bounds.bottom)};
                mGradientShader = new LinearShader(points, mGradientDistributions, mGradientColors, Shader.TileMode.CLAMP_TILEMODE);
                updateGradientAngle(mGradientAngle);
                mProgressPaint.setShader(mGradientShader, Paint.ShaderType.LINEAR_SHADER);
                break;

            case GRADIENT_RADIAL:
                mGradientShader = new RadialShader(new Point(bounds.getCenter().getPointX(), bounds.getCenter().getPointY()),
                        bounds.bottom - bounds.getCenter().getPointY(), mGradientDistributions,
                        mGradientColors, Shader.TileMode.MIRROR_TILEMODE);
                mProgressPaint.setShader(mGradientShader, Paint.ShaderType.RADIAL_SHADER);
                break;
        }
    }

    /**
     * Update drawing angles
     */
    @Override
    void updateDrawingAngles() {
        float height = mRadius - mProgress * (mRadius * 2) / DEFAULT_MAX;
        double radiusPow = Math.pow(mRadius, 2);
        double heightPow = Math.pow(height, 2);

        mSweepAngle = (height == 0) ? 180 : (float) Math.toDegrees(Math.acos((heightPow + radiusPow - Math.pow(Math.sqrt(radiusPow - heightPow), 2)) / (2 * height * mRadius))) * 2;
        mStartAngle = mDirectionAngle - (mSweepAngle / 2);
        mBgSweepAngle = (mBackgroundOffset > 0) ? 360 : mSweepAngle - 360;
    }

    /**
     * Update gradient angle *
     *
     * @param angle angle
     */
    @Override
    void updateGradientAngle(float angle) {
        if (mGradientType == -1 || mGradientType == GRADIENT_RADIAL) {
            return;
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(angle, mCircleBounds.getCenter().getPointX(), mCircleBounds.getCenter().getPointY());
        mGradientShader.setShaderMatrix(matrix);
    }

    /**
     * Get start angle float
     *
     * @return the float
     */
    @Override
    public float getStartAngle() {
        return mDirectionAngle;
    }

    /**
     * Set start angle *
     *
     * @param angle angle
     */
    @Override
    public void setStartAngle(float angle) {
        if (this.mDirectionAngle == angle) {
            return;
        }
        this.mDirectionAngle = angle;
        updateDrawingAngles();
    }

    /**
     * Get background offset int
     *
     * @return the int
     */
    public int getBackgroundOffset() {
        return mBackgroundOffset;
    }

    /**
     * Set background offset *
     *
     * @param backgroundOffset background offset
     */
    public void setBackgroundOffset(int backgroundOffset) {
        if (!mDrawBackground || this.mBackgroundOffset == backgroundOffset) {
            return;
        }
        this.mBackgroundOffset = backgroundOffset;
        measureBackgroundBounds();
        updateDrawingAngles();
    }
}
