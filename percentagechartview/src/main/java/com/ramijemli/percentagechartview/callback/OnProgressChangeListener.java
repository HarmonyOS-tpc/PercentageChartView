package com.ramijemli.percentagechartview.callback;

/**
 * On progress change listener
 */
public interface OnProgressChangeListener {
    /**
     * On progress changed *
     *
     * @param progress progress
     */
    void onProgressChanged(float progress);

}
