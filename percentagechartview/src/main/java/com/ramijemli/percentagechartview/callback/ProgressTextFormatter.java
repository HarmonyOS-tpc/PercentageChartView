package com.ramijemli.percentagechartview.callback;

/**
 * Progress text formatter
 */
public interface ProgressTextFormatter {
    /**
     * Provide formatted text char sequence
     *
     * @param progress progress
     * @return the char sequence
     */
    CharSequence provideFormattedText(float progress);
}
