package com.ramijemli.percentagechartview.callback;

/**
 * Adaptive color provider
 */
public interface AdaptiveColorProvider {
    /**
     * Provide progress color int
     *
     * @param progress progress
     * @return the int
     */
    default int provideProgressColor(float progress) {
        return -1;
    }

    /**
     * Provide background color int
     *
     * @param progress progress
     * @return the int
     */
    default int provideBackgroundColor(float progress) {
        return -1;
    }

    /**
     * Provide text color int
     *
     * @param progress progress
     * @return the int
     */
    default int provideTextColor(float progress) {
        return -1;
    }

    /**
     * Provide background bar color int
     *
     * @param progress progress
     * @return the int
     */
    default int provideBackgroundBarColor(float progress) {
        return -1;
    }

}
