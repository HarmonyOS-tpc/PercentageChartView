package com.ramijemli.percentagechartview;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * Author White
 * Date 2017/4/23
 * Time 14:27
 */
public class Utils {
    /**
     * Dp 2 px int
     *
     * @param context context
     * @param dpVal   dp val
     * @return the int
     */
    public static int dp2px(Context context, int dpVal) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        final float scale = displayAttributes.densityPixels;
        return (int) (dpVal * scale + 0.5f);
    }

    /**
     * Sp 2 px int
     *
     * @param context context
     * @param spVal   sp val
     * @return the int
     */
    public static int sp2px(Context context, int spVal) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        final float fontScale = displayAttributes.scalDensity;
        return (int) (spVal * fontScale + 0.5f);
    }
}
