该三方开源库从github fork过来，主要将底层接口调用的实现修改成鸿蒙接口的实现，将三方库鸿蒙化，供开发鸿蒙应用的开发者使用

fork地址：https://github.com/RamiJ3mli/PercentageChartView

fork版本号/日期：0.3.1 / 2019/5/14


# PercentageChartView

PercentageChartView一个百分比图表，显示任何给定任务或信息的进度

原项目Readme地址：https://github.com/RamiJ3mli/PercentageChartView/blob/master/README.md

项目移植状态：支持组件所有基本功能

完成度：100%

调用差异：无





## 导入方法

**har导入**

将har包放入lib文件夹并在build.gradle添加

```
implementation fileTree(dir: 'libs', include: ['*.har'])
```

**Library引用**

添加本工程中的模块到任意工程中，在需要使用的模块build.gradle中添加

```
compile project(path: ':percentagechartview')
```

or

```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:PercentageChartView:1.0.1'
```

## 使用方法

##### XML
```xml
<com.ramijemli.percentagechartview.PercentageChartView
        ohos:id="$+id:pie_chart"
        ohos:height="160vp"
        ohos:width="match_parent"
        ohos:margin="4vp"
        ohos:pcv_gradientColors="#ff00ff00,#ff0000ff"
        ohos:pcv_gradientType="0"
        ohos:pcv_mode="1"
        ohos:pcv_progress="70"
        ohos:pcv_startAngle="90"
        ohos:pcv_textSize="20fp"
        ohos:pcv_typeface="interstellar.ttf"
        ohos:weight="1"/>

<com.ramijemli.percentagechartview.PercentageChartView
        ohos:id="$+id:ring_chart"
        ohos:height="160vp"
        ohos:width="match_parent"
        ohos:margin="4vp"
        ohos:pcv_backgroundBarThickness="30vp"
        ohos:pcv_gradientColors="#ff00ff00,#ff0000ff"
        ohos:pcv_gradientType="2"
        ohos:pcv_mode="0"
        ohos:pcv_progress="70"
        ohos:pcv_progressBarThickness="30vp"
        ohos:pcv_startAngle="90"
        ohos:pcv_textSize="20fp"
        ohos:pcv_typeface="interstellar.ttf"
        ohos:weight="1"/>

<com.ramijemli.percentagechartview.PercentageChartView
        ohos:id="$+id:fill_chart"
        ohos:height="160vp"
        ohos:width="match_parent"
        ohos:margin="4vp"
        ohos:pcv_gradientColors="#ff00ff00,#ff0000ff"
        ohos:pcv_gradientType="1"
        ohos:pcv_mode="2"
        ohos:pcv_progress="70"
        ohos:pcv_startAngle="90"
        ohos:pcv_textSize="20fp"
        ohos:pcv_typeface="interstellar.ttf"
        ohos:weight="1"/>
```

##### Java

```java
//初始化
fillPercentageChartView = (PercentageChartView) findComponentById(ResourceTable.Id_fill_chart);
piePercentageChartView = (PercentageChartView) findComponentById(ResourceTable.Id_pie_chart);
ringPercentageChartView = (PercentageChartView) findComponentById(ResourceTable.Id_ring_chart);

```

## 一些功能设置介绍
```
//设置是否化背景
PercentageChartView.setDrawBackgroundEnabled(boolean enabled)
//设置是否画背景进度
PercentageChartView.setDrawBackgroundBarEnabled(boolean enabled)
//设置背景进度的颜色
PercentageChartView.setBackgroundBarColor(int color)
//设置背景颜色
PercentageChartView.setBackgroundColor(int color)
//设置背景偏移
PercentageChartView.setBackgroundOffset(int offset)
//设置渲染方式，渲染颜色，渲染的方向
PercentageChartView.setGradientColors(@GradientTypes int type, Color[] colors, float[] positions, float angle)
//设置进度的方向
PercentageChartView.setOrientation(@ProgressOrientation int orientation)
//设置进度
PercentageChartView.setProgress(float progress, boolean animate)
//设置进度条的样式
PercentageChartView.setProgressBarStyle(@ProgressBarStyle int style)
//设置进度条的颜色
PercentageChartView.setProgressColor(int color)
//设置进度条的开始角度
PercentageChartView.setStartAngle(float startAngle)
//设置文字的颜色
PercentageChartView.setTextColor(int color)
//设置文字的阴影
PercentageChartView.setTextShadow(int shadowColor, float shadowRadius, float shadowDistX, float shadowDistY)
//设置文字的大小
PercentageChartView.setTextSize(float size)
//设置文字的字体
PercentageChartView.setTypeface(Font.Builder typeface)
```



