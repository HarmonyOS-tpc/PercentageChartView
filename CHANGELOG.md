1.0.0

Initial Release

Support Features:
--------------------------------------------
1) 设置是否化背景
PercentageChartView.setDrawBackgroundEnabled(boolean enabled)
1) 设置是否画背景进度
PercentageChartView.setDrawBackgroundBarEnabled(boolean enabled)
1) 设置背景进度的颜色
PercentageChartView.setBackgroundBarColor(int color)
1) 设置背景颜色
PercentageChartView.setBackgroundColor(int color)
1) 设置背景偏移
PercentageChartView.setBackgroundOffset(int offset)
1) 设置渲染方式，渲染颜色，渲染的方向
PercentageChartView.setGradientColors(@GradientTypes int type, Color[] colors, float[] positions, float angle)
1) 设置进度的方向
PercentageChartView.setOrientation(@ProgressOrientation int orientation)
1) 设置进度
PercentageChartView.setProgress(float progress, boolean animate)
1) 设置进度条的样式
PercentageChartView.setProgressBarStyle(@ProgressBarStyle int style)
1) 设置进度条的颜色
PercentageChartView.setProgressColor(int color)
1) 设置进度条的开始角度
PercentageChartView.setStartAngle(float startAngle)
1) 设置文字的颜色
PercentageChartView.setTextColor(int color)
1) 设置文字的阴影
PercentageChartView.setTextShadow(int shadowColor, float shadowRadius, float shadowDistX, float shadowDistY)
1) 设置文字的大小
PercentageChartView.setTextSize(float size)
1) 设置文字的字体
PercentageChartView.setTypeface(Font.Builder typeface)

Not Support Features:
--------------------------------------------------------

无